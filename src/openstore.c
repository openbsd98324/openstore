

/*
 * Copyright (c) 2008 The NetBSD Foundation, Inc.
 *
 * This code is derived from software contributed to The NetBSD Foundation
 * by 
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE NETBSD FOUNDATION, INC. AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */






#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#include <dirent.h>
#include <time.h>
#if defined(__linux__) //linux
#define MYOS 1
#elif defined(_WIN32)
#define MYOS 2
#elif defined(_WIN64)
#define MYOS 3
#elif defined(__unix__) 
#define MYOS 4  // freebsd
#define PATH_MAX 2500
#else
#define MYOS 0
#endif
#include <ctype.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h> 
#include <time.h>
#define KRED  "\x1B[31m"
#define KGRE  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KNRM  "\x1B[0m"
#define KBGRE  "\x1B[92m"
#define KBYEL  "\x1B[93m"
int os_bsd_type = 0;



void nsystem( char *mycmd )
{
	char foocharo[PATH_MAX];
	printf( "<CMD: %s>\n", mycmd );
	snprintf( foocharo , sizeof( foocharo ), "%s" , mycmd ); 
	system(   foocharo ); 
	printf( " Defunc CMD: %s>\n", mycmd );
}


void ncmdwith( char *mycmd, char *myfile )
{
	char cmdi[PATH_MAX];
	printf( "** CMD (start) (OS: %d)\n" , MYOS );
	char foocharo[PATH_MAX];
	snprintf( foocharo , sizeof( foocharo ), " %s \"%s\" " , mycmd , myfile ); 
	nsystem(   foocharo ); 
	printf( "** CMD (completed) (OS: %d)\n" , MYOS );
}








int fexist(const char *a_option)
{
	char dir1[PATH_MAX]; 
	char *dir2;
	DIR *dip;
	strncpy( dir1 , "",  PATH_MAX  );
	strncpy( dir1 , a_option,  PATH_MAX  );

	struct stat st_buf; 
	int status; 
	int fileordir = 0 ; 

	status = stat ( dir1 , &st_buf);
	if (status != 0) {
		fileordir = 0;
	}
	FILE *fp2check = fopen( dir1  ,"r");
	if( fp2check ) {
		fileordir = 1; 
		fclose(fp2check);
	} 

	if (S_ISDIR (st_buf.st_mode)) {
		fileordir = 2; 
	}
	return fileordir;
}




void fetch_file_ftp( char *pattern )
{
	char foocwd[PATH_MAX];
	int fetch_file_ftp_force_httpwget = 0; 
	int fetch_file_ftp_force_bsdwget  = 0; 
	printf( "=========================\n" );
	printf( " ===  Fetch File FTP === \n" );
	printf( "=========================\n" );
	printf( "PWD: %s\n", getcwd( foocwd , PATH_MAX ) );
	printf( " ...\n" );

	if ( fexist( "/etc/fetch_httpwget" ) == 1 ) 
		fetch_file_ftp_force_httpwget = 1; 
	if ( fexist( "/etc/fetch_bsdwget" ) == 1 ) 
		fetch_file_ftp_force_bsdwget = 1; 


	if ( MYOS != 1 )  // BSD
		ncmdwith( " ftp  " ,  pattern   );
	else if ( MYOS == 1 )  // linux
		ncmdwith( " wget -c --no-check-certificate  " ,  pattern   );


	/// rescue for ssl handshake
	if ( fetch_file_ftp_force_bsdwget == 1) 
	{
		printf( " => FORCE\n"); 
		ncmdwith( "  /usr/pkg/bin/wget -c --no-check-certificate " ,  pattern   );
	}
	else if ( fetch_file_ftp_force_httpwget == 1) 
	{
		printf( " => FORCE\n"); 
		ncmdwith( " httpwget  " ,  pattern   );
	}

	printf( " ...\n" );
	printf( "======================\n" );
}












#include <stdio.h>
#include <unistd.h>
int main( int argc, char *argv[])
{

	char charo[PATH_MAX];
	char cmdi[PATH_MAX];


	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "store" ) ==  0 )   
	|| ( strcmp( argv[1] ,   "get"   ) ==  0 )  
	|| ( strcmp( argv[1] ,   "wget"  ) ==  0 )  
	|| ( strcmp( argv[1] , "store-links" ) ==  0 )  )   /// rescue 
	{

			snprintf(      charo , sizeof( charo ),  "https://gitlab.com/openbsd98324/%s/-/archive/main/%s-main.tar.gz" , argv[ 2 ] , argv[ 2 ] );
			printf( "URL/FILE: %s\n", charo );
			fetch_file_ftp( charo );

			snprintf(      charo , sizeof( charo ),  "https://gitlab.com/openbsd98324/%s/-/archive/master/%s-master.tar.gz" , argv[ 2 ] , argv[ 2 ] );
			printf( "URL/FILE: %s\n", charo );
			fetch_file_ftp( charo );
			// main works 
			if ( strcmp( argv[1] , "store-links" ) ==  0 )   /// rescue  for ssl key handshake
			{
				snprintf(      charo , sizeof( charo ),  "links https://gitlab.com/openbsd98324/%s/-/archive/main/%s-main.tar.gz" , argv[ 2 ] , argv[ 2 ] );
				printf( "URL/FILE: %s\n", charo );
				nsystem( charo );
			}

			snprintf(  charo , sizeof( charo ),  "  tar xvpfz %s-master.tar.gz" , argv[ 2 ] ); 
			nsystem( charo ); 

			snprintf(  charo , sizeof( charo ),  "  tar xvpfz %s-main.tar.gz" , argv[ 2 ] ); 
			nsystem( charo ); 

			return 0; 
		}







	if ( argc == 3 )
		if ( strcmp( argv[1] , "git" ) ==  0 ) 
		{
			strncpy( cmdi, "  git clone https://gitlab.com/openbsd98324/", PATH_MAX );
			strncat( cmdi , argv[2] , PATH_MAX - strlen( cmdi ) -1 );
			strncat( cmdi , "    "  , PATH_MAX - strlen( cmdi ) -1 );
			nsystem( cmdi );
			return 0;
		}





	printf(" == Util: Bye! ==\n");
	return 0;
}


// EOF  



